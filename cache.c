


#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

// usage: ./a.out <file_name>
// reads th first byte from every page
// utilty to ensure the entire file is cached
int main(int argc, char* argv[]) {
        char *filename = argv[1];
        int fd;
        char *buf;
        struct stat filestat;
        int size = 1024*1024*1024;
        fd = open(filename, O_RDONLY);
        if (fd == -1) {
                fprintf(stderr,"Error opening file %s \n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        buf = mmap(NULL,size, PROT_READ, MAP_SHARED,fd,0);
        if(buf == MAP_FAILED) {
                fprintf(stderr,"Error mapping file %s \n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        // read the first byte from every page;
        size_t i;
        char temp;
        for(i=0;i<size; i+= sysconf(_SC_PAGESIZE)){
               temp = buf[i];
        }


        if(munmap(buf,size)==-1) {
                fprintf(stderr,"Error running munmap %s \n", strerror(errno));
                exit(EXIT_FAILURE);
        }
        return 0;

}

