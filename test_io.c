#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>

#define GB 1024*1024*1024
#define IO_SIZE 4096
#define SEED 15

void shuffle(uint64_t *array, size_t n)
{
  if (n > 1) {
    size_t i;
    for (i = 0; i < n - 1; i++) {
      size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
      uint64_t t = array[j];
      array[j] = array[i];
      array[i] = t;
    }
  }
}
int main(int argc, char* argv[]) {
        srand(SEED);

        // read from command line

        char* TEST_FILE  = argv[1];

        struct timespec start_time, end_time;
        size_t size = GB;
        char* buf = valloc(IO_SIZE);
        size_t offset_array[size/IO_SIZE];
        int fd = open(TEST_FILE, O_DIRECT| O_RDWR);


        // experiment parameters:i
        // opt_read: 1 if read 0 if write
        // sequential: 1 if sequential r/w 0 if random
        int opt_read = atoi(argv[2]);
        int sequential = atoi(argv[3]);

        // construct offset_array and shuffle if applicable
        int s;
        int n = 0;
        for(s=0;s<size;s+=IO_SIZE) {
                offset_array[n++] = s;

        }
        if (!sequential) {
                shuffle(offset_array,n);
        }

        // begin the file i/o
        // Using the function given in Lab1
        // start timer here to measure the file i/o
        printf("Starting file i/o opt_read=%d sequential=%d \n",opt_read,sequential);
        clock_gettime(CLOCK_MONOTONIC, &start_time);

        int ret = 0;
        int i;
        for(i=0;i<n;i++) {
                ret = lseek(fd,offset_array[i], SEEK_SET);
                if (ret == -1) {
                        perror("lseek");
                        exit(-1);
                }
                if (opt_read)
                        ret = read(fd, buf, IO_SIZE);
                else
                        ret = write(fd, buf, IO_SIZE);
                if (ret == -1) {
                        perror("read/write");
                        exit(-1);
                }
        }
        clock_gettime(CLOCK_MONOTONIC, &end_time);
        free(buf);
        double time_elapsed;
        time_elapsed = (end_time.tv_sec - start_time.tv_sec)* 1e9;
        time_elapsed =  (time_elapsed + (end_time.tv_nsec - start_time.tv_nsec)) * 1e-9;
        printf("elapsed time(sec): %lf \n",time_elapsed);
        return 0;


}

