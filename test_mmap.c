#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define GB (1024*1024*1024)
#define SEED 15



void shuffle(uint64_t *array, size_t n)
{
  if (n > 1) {
    size_t i;
    for (i = 0; i < n - 1; i++) {
      size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
      uint64_t t = array[j];
      array[j] = array[i];
      array[i] = t;
    }
  }
}

// usage:
// ./a.out <filename> <0=anon,1=file> <0=private,1=shared>
int main(int argc, char* argv[]) {
        struct timespec start_time, end_time;
        clock_gettime(CLOCK_MONOTONIC, &start_time);

        // set the seed to make RAND consistent across runs
        srand(SEED);

        // mmap 1GB
        char* TEST_FILE  = argv[1];
        size_t size = GB;
        char* ptr;
        int fd = open(TEST_FILE,O_RDWR);


        bool file = atoi(argv[2]);
        bool shared = atoi(argv[3]);

        int MAPPING = shared ? MAP_SHARED : MAP_PRIVATE;
        if (file){
                ptr = mmap(NULL,GB, PROT_READ|PROT_WRITE, MAPPING, fd,0);
        } else {
                ptr = mmap(NULL,size, PROT_READ |PROT_WRITE, MAPPING | MAP_ANONYMOUS,-1,0);
        }
        if (ptr == MAP_FAILED) {
                fprintf(stderr, "Failed to get mmap region: %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }


        // Generates the indices to be used then shuffles them

        size_t pages = size/sysconf(_SC_PAGESIZE);
        size_t order[pages];
        size_t i;
        size_t order_i = 0;
        for(i=0;i<size; i+= sysconf(_SC_PAGESIZE)){
                order[order_i] = i;
                order_i++;
        }
        shuffle(order,pages);

        // Perform the writes
        for(i=0;i<pages;i++){
                ptr[order[i]] = 't';
        }


        // clean up the memory
        msync(ptr,size,MS_SYNC);

        if(munmap(ptr,size)==-1){
                fprintf(stderr, "Failed to free region: %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        if(close(fd)==-1){
                fprintf(stderr, "Failed to close fd %s\n", strerror(errno));
                exit(EXIT_FAILURE);

        }
         clock_gettime(CLOCK_MONOTONIC, &end_time);


        double time_elapsed;
        time_elapsed = (end_time.tv_sec - start_time.tv_sec)* 1e9;
        time_elapsed =  (time_elapsed + (end_time.tv_nsec - start_time.tv_nsec)) * 1e-9;
        printf("elapsed time(sec): %lf \n",time_elapsed);

        return 0;
}

